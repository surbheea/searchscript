import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class SeachPage {  // class name

	Actions action; //class
	public SeachPage(WebDriver driver1) { // Constructor for the class , WebDriver passed as argument
		action = new Actions(driver1); // object created
		
		
	}
	
	public void PressEnter() {  // Method created to press enter. It did not retun any value
		action.sendKeys(Keys.ENTER).build().perform();
		
	}
	public void ScrollDown(WebDriver driver1) { // Method.To scroll down use this code.because ojb for javascript cannot be created, so assign the obj to it.
		JavascriptExecutor executor = (JavascriptExecutor) driver1;
		executor.executeScript("window.scrollBy(0,2000)", "");
		
	}
	
	public void PerformMouseover(WebElement element1) { // Method
		action.moveToElement(element1).build().perform();
		
	}
	public void Click(WebElement element1, WebDriver driver1) { // Method
		JavascriptExecutor executor = (JavascriptExecutor) driver1;
		executor.executeScript("arguments[0].click();", element1);
		
		
	}
	public static void main(String[] args) {  // Main Method
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","/Users/surbheeagrawal/Downloads/chromedriver");
		WebDriver driver1 = new ChromeDriver();// launch chrome
		SeachPage objSearch = new SeachPage(driver1);// Object created for search class
		driver1.manage().window().maximize();
		driver1.manage().deleteAllCookies();
		// dynamic wait
		driver1.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		driver1.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver1.get("https://www.dollardays.com/"); // launch url
	    driver1.findElement(By.name("terms")).sendKeys("masks");
	    objSearch.PressEnter(); // calling PressEnter method using objsearch.
	    objSearch.ScrollDown(driver1); // use of method
	    
	    
	    
	    
//driver1.quit();
	}}
